import java.util.ArrayList;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.File;
import java.util.Collections;
import java.util.Scanner;
import java.io.IOException;

public class Laboratorio{
    private ArrayList<Computer> laboratorio;

    public Laboratorio() {
        laboratorio = new ArrayList<Computer>();
    }


    public Laboratorio(Laboratorio l) {
        laboratorio = new ArrayList<Computer>();
        for (int i=0; i<l.getSize(); i++) {
            laboratorio.add(((Desktop)l.getComputer(i)).clone());
        }
    }


    public Computer getComputer(int i){
        if(i<laboratorio.size()){
            return laboratorio.get(i);
        }else
            return null;
    }


    public int getSize(){ return laboratorio.size();}

    public void save(String fileName){
        try{
            final FileOutputStream f = new FileOutputStream(new File(fileName));
            final ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(laboratorio);
            o.close();
            f.close();
            System.out.println("il salvataggio nel file "+fileName+" è stato completato");
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }

    @SuppressWarnings("unchecked")
    public void load(String fileName) {
        try {
            File f = new File(fileName);
            if(f.exists()){
                final FileInputStream fi = new FileInputStream(f);
                final ObjectInputStream oi  = new ObjectInputStream(fi);
                laboratorio = (ArrayList<Computer>) oi.readObject();
                fi.close();
                oi.close();
                System.out.println("caricamento dal file "+fileName+" completato");
            }else{
                System.out.println("il file non è stato trovato");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadCSV(final String nomefile) throws IOException {
        Scanner scanner = new Scanner(new File(nomefile)).useDelimiter(System.lineSeparator());
        try {
            while (scanner.hasNext()) {
                final String[] valori = scanner.next().split(";");

                if(valori[0].matches("Desktop")){
                    laboratorio.add(new Desktop(valori[1],valori[2],valori[3],Boolean.valueOf(valori[4]),Double.valueOf(valori[5])));
                }
            }
        } catch (Exception e) {
        } finally{
            scanner.close();
        }
    }


    public void elencoPCguasti() {
        for (Computer i : laboratorio) {
            if (i.getGuasto()) {
                System.out.println(i.toString());
            }
        }
    }

    public void elencoPC() {
        for (Computer i : laboratorio) {
            System.out.println(i.toString());
        }
    }

    public boolean getIsEmpty(){return laboratorio.isEmpty();}


    @Override
    public String toString(){
        String elenco="";

        for(Computer i: laboratorio){
            elenco+=i.toString();
        }

        return elenco;
    }

    public void add(Computer p) {
        laboratorio.add(p);
    }


    public void add(Computer p,int index){
        if(index>-1 && index<laboratorio.size())
            laboratorio.set(index,p);
        else
            laboratorio.add(p);
    }

    public void remove(Computer p) {
        if(laboratorio.size()>0){
            ArrayList<Computer> laboratorioCopia = new ArrayList<Computer>();
            for(Computer i:laboratorio){
                if(!((Desktop)i).equals((Desktop)p)){
                    laboratorioCopia.add((Desktop)i.clone());
                }
            }

            laboratorio=laboratorioCopia;
        }
    }

    public void remove(int index){
        if(index>-1 && index<laboratorio.size())
            laboratorio.remove(index);
    }

    public void sort(){
        Collections.sort(laboratorio);
    }
}
