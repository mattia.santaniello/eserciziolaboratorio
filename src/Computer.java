public abstract class Computer implements java.io.Serializable,Comparable<Computer>{
    private static final long serialVersionUID = 1L;
    private String marca;
    private String tipo;
    private String memoria;

    private boolean guasto;

    public Computer(String marca,String memoria,String tipo,boolean guasto){
        this.marca=marca;
        this.tipo=tipo;
        this.memoria=memoria;
        this.guasto=guasto;
    }

    public Computer(Computer p){
        this.marca=p.getMarca();
        this.tipo=p.getTipo();
        this.memoria=p.getMemoria();
        this.guasto=p.getGuasto();
    }


    public String getMarca(){ return this.marca;}
    public String getTipo(){return this.tipo;}
    public String getMemoria(){return this.memoria;}
    public boolean getGuasto(){return this.guasto;}


    public void setMarca(String marca){ this.marca=marca;}
    public void setTipo(String tipo){this.tipo = tipo;}
    public void setMemoria(String memoria){this.memoria=memoria;}
    public void setGuasto(boolean guasto){this.guasto=guasto;}

    @Override
    public String toString(){
        return "\nmarca:"+marca+"\ntipo:"+tipo+"\nmemoria:"+memoria+"\nguasto:"+guasto+"\n";
    }

    public boolean equals(Computer p){
        return p.getMarca().matches(marca) && p.getTipo().matches(tipo) && p.getMemoria().matches(memoria) && p.getGuasto() == guasto;
    }

    public abstract Computer clone();

    @Override
    public int compareTo(Computer p){
        return marca.compareTo( p.getMarca());
    }
}

class Desktop extends Computer {
    private static final long serialVersionUID = 1L;
    private double pollici;

    public Desktop(String marca,String memoria,String tipo,boolean guasto,double pollici){
        super( marca, memoria, tipo, guasto);
        this.pollici=pollici;
    }

    public Desktop(Desktop d){
        super(d);
        this.pollici=d.getPollici();
    }

    public double getPollici(){return pollici;}
    public void setPollici(double pollici){this.pollici=pollici;}

    @Override
    public boolean equals(Object o){
        boolean result=false;

        if(o!=null){
            if(o instanceof Desktop)
                result= super.equals((Computer)o) && ((Desktop)o).getPollici() == pollici;
        }
        
        return result;
    }

    @Override
    public String toString(){
        return super.toString()+"pollici:"+pollici+"\n";
    }

    @Override
    public Desktop clone(){
        return new Desktop(this);
    }
}