
public class App {
    public static void main(String[] args) throws Exception {
        Computer pc1 = new Desktop("HP","3000 S","4 GB",false,8.1);
        Computer pc2 = new Desktop("Lenovo","Thinkpad 500","16 GB",true,20.1);
        Computer pc3 = new Desktop("Acer","Aspire One","8 GB",false,17.12);

        Laboratorio l = new Laboratorio();

        l.add(pc1);
        l.add(pc2);
        l.add(pc3);

        l.elencoPC();
        l.remove(pc1);
        l.save("backup");
        
        l.load("backup");
        l.add(new Desktop("Asus","Zephire","8 GB",false,18.3));
        l.sort();
        l.elencoPC();
    }
}